package com.example.appbares.ui.favorito

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appbares.R
import com.example.appbares.model.AdminSQLiteHelper
import com.example.appbares.model.BarRecyclerView
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_favorito.*

class FavoritoFragment : Fragment() {

    lateinit var fRecyclerView: RecyclerView
    val fAdapter : FavoritoAdapter = FavoritoAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_favorito, container, false)

        return root
    }

    private fun getFavoritos() {
        val favoritos: MutableList<BarRecyclerView> = mutableListOf()
        refreshLayout_Favoritos.isRefreshing = true
        val admin = AdminSQLiteHelper(context!!, "administracion", null, 1)
        val bd = admin.writableDatabase
        val firebaseAuth = FirebaseAuth.getInstance().currentUser?.uid
        val fila = bd.rawQuery("select * from favorito where usuario='${firebaseAuth.toString()}'", null)
        while (fila.moveToNext()) {
            val favorito = BarRecyclerView(
                fila.getString(0),
                fila.getString(2),
                fila.getString(3),
                fila.getDouble(4),
                fila.getString(5),
                fila.getString(6)
            )
            favoritos.add(favorito)
        }
        refreshLayout_Favoritos.isRefreshing = false
        favoritos?.let {
            showFavoritos(it)
        }
    }

    private fun showFavoritos(favoritos: MutableList<BarRecyclerView>) {
        fRecyclerView.setHasFixedSize(true)
        fAdapter.RecyclerAdapter(favoritos, activity!!)
        fRecyclerView.adapter = fAdapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fRecyclerView = view.findViewById(R.id.recyclerView_Favorito) as RecyclerView
        fRecyclerView.layoutManager = LinearLayoutManager(activity)
        fRecyclerView.adapter = fAdapter

        refreshLayout_Favoritos.setOnRefreshListener {
            getFavoritos()
        }

        getFavoritos()
    }
}