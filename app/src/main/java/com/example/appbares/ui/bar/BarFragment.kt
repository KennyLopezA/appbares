package com.example.appbares.ui.bar

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appbares.R
import com.example.appbares.model.BarRecyclerView
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_bar.*

class BarFragment : Fragment() {

    lateinit var bRecyclerView: RecyclerView
    val bAdapter : BarAdapter = BarAdapter()
    val db = FirebaseFirestore.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_bar, container, false)

        return root
    }

    private fun getBares() {
        val bares: MutableList<BarRecyclerView> = mutableListOf()
        refreshLayout_Bares.isRefreshing = true
        db.collection("bares")
            .addSnapshotListener{snapshot, error ->
                if (error != null) {
                    refreshLayout_Bares.isRefreshing = false
                    return@addSnapshotListener
                }

                for (doc in snapshot!!.documentChanges) {
                    when(doc.type) {
                        DocumentChange.Type.ADDED -> {
                            val bar = BarRecyclerView(
                                doc.document.id,
                                doc.document.getString("nombre")!!,
                                doc.document.getString("imagen")!!,
                                doc.document.getDouble("estrellas")!!,
                                doc.document.getString("direccion")!!,
                                doc.document.getString("telefono")!!
                            )
                            bares.add(bar)
                        }
                        else -> return@addSnapshotListener
                    }
                }
                refreshLayout_Bares.isRefreshing = false
                bares?.let {
                    showBares(it)
                }
            }
    }

    private fun showBares(bares: MutableList<BarRecyclerView>) {
        bRecyclerView.setHasFixedSize(true)
        bAdapter.RecyclerAdapter(bares, activity!!)
        bRecyclerView.adapter = bAdapter
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bRecyclerView = view.findViewById(R.id.recyclerView_Bar) as RecyclerView
        bRecyclerView.layoutManager = GridLayoutManager(this.context, 2, RecyclerView.VERTICAL, false)
        bRecyclerView.adapter = bAdapter


        refreshLayout_Bares.setOnRefreshListener {
            getBares()
        }
        getBares()
    }

}