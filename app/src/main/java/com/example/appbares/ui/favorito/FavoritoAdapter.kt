package com.example.appbares.ui.favorito

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appbares.R
import com.example.appbares.model.BarRecyclerView
import com.squareup.picasso.Picasso

class FavoritoAdapter : RecyclerView.Adapter<FavoritoAdapter.FavoritoViewHolder>() {

    var favoritos: MutableList<BarRecyclerView> = mutableListOf()
    lateinit var context: Context

    fun RecyclerAdapter(bares: MutableList<BarRecyclerView>, context: Context) {
        this.favoritos = bares
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoritoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return FavoritoViewHolder(layoutInflater.inflate(R.layout.layout_favorito, parent, false))
    }

    override fun getItemCount(): Int {
        if (favoritos != null) {
            return favoritos.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: FavoritoViewHolder, position: Int) {
        val item = favoritos[position]
        holder.bind(item, context)
    }

    class FavoritoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombreFavorito = view.findViewById(R.id.tv_name_Fav) as TextView
        val imagenFavorito = view.findViewById(R.id.imv_Fav) as ImageView
        val verFavorito = view.findViewById(R.id.imb_VerFav) as ImageButton

        fun bind(favoritos: BarRecyclerView, context: Context) {
            nombreFavorito.text = favoritos.nombre
            if (favoritos.imagen != "") {
                Picasso.with(imagenFavorito.context).load(favoritos.imagen).into(imagenFavorito)
            }

            val dialog = Dialog(context)
            dialog.setContentView(R.layout.layout_information_bar)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val dialogNombre = dialog.findViewById(R.id.tv_nombre) as TextView
            val dialogClasificacion = dialog.findViewById(R.id.tv_clasificacion) as TextView
            val dialogDireccion = dialog.findViewById(R.id.tv_direccion) as TextView
            val dialogTelefono = dialog.findViewById(R.id.tv_telefono) as TextView


            verFavorito.setOnClickListener {
                dialogNombre.text = favoritos.nombre
                dialogClasificacion.text = favoritos.estrellas.toString()
                dialogDireccion.text = favoritos.direccion
                dialogTelefono.text = favoritos.telefono
                dialog.show()
            }
        }
    }

}