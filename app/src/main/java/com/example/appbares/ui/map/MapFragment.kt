package com.example.appbares.ui.map

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.appbares.R
import com.example.appbares.model.Bar
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.FirebaseFirestore

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var db: FirebaseFirestore
    private lateinit var tmpParkingMarkers: MutableList<Marker>
    private lateinit var realParkingMarkers: MutableList<Marker>
    private lateinit var botonUber: ImageButton
    private var bares: Bar? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_map, container, false)

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        db = FirebaseFirestore.getInstance()
        tmpParkingMarkers = mutableListOf()
        realParkingMarkers = mutableListOf()
        botonUber = root.findViewById(R.id.ib_Uber) as ImageButton

        botonUber.setOnClickListener {
            val location =
                Uri.parse("geo:0,0")
            val intent = Intent(Intent.ACTION_VIEW, location)
            val chooser = Intent.createChooser(intent, "UBER")

            if (intent.resolveActivity(activity!!.packageManager) != null) {
                startActivity(chooser)
            }
        }

        getBares()

        return root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.uiSettings.isZoomControlsEnabled = true
        map.setOnMarkerClickListener(this)

        setUpMap()
    }

    override fun onMarkerClick(p0: Marker?) = false

    private fun placeMarkerOnMap(location: LatLng) {
        // 1
        val markerOptions = MarkerOptions().position(location)
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(resources, R.mipmap.ic_user_location)))
        // 2
        map.addMarker(markerOptions)
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }
        // 1
        map.isMyLocationEnabled = true

        // 2
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            // 3
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))

            }
        }
    }

    private fun getBares() {
        realParkingMarkers.forEach {
            it.remove()
        }
        db.collection("bares")
            .get()
            .addOnSuccessListener { result ->
                val markerOptions = MarkerOptions()
                result.forEach {
                    bares = it.toObject(Bar::class.java)
                    markerOptions.position(
                        LatLng(
                            bares?.coordenada!!.latitude,
                            bares?.coordenada!!.longitude
                        )
                    )
                    markerOptions.title(bares?.nombre)
                    tmpParkingMarkers.add(map.addMarker(markerOptions))
                }
            }
            .addOnFailureListener{ exception ->
                Log.e("error", "ERROR: $exception")
            }
        realParkingMarkers.clear()
        realParkingMarkers.addAll(tmpParkingMarkers)
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

}
