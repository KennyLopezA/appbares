package com.example.appbares.ui.bar

import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.appbares.R
import com.example.appbares.model.AdminSQLiteHelper
import com.example.appbares.model.BarRecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso

class BarAdapter : RecyclerView.Adapter<BarAdapter.BarViewHolder>() {

    var bares: MutableList<BarRecyclerView> = mutableListOf()
    lateinit var context: Context

    fun RecyclerAdapter(bares: MutableList<BarRecyclerView>, context: Context) {
        this.bares = bares
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BarViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return BarViewHolder(layoutInflater.inflate(R.layout.layout_bar, parent, false))
    }

    override fun getItemCount(): Int {
        if (bares != null) {
            return bares.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: BarViewHolder, position: Int) {
        val item = bares[position]
        holder.bind(item, context)
    }

    class BarViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val nombreBar = view.findViewById(R.id.tv_name_Bar) as TextView
        val imagenBar = view.findViewById(R.id.imv_Bar) as ImageView
        val favoritoBar = view.findViewById(R.id.imb_Fav) as ImageButton
        val verBar = view.findViewById(R.id.imb_Ver) as ImageButton
        val firebaseAuth = FirebaseAuth.getInstance().currentUser?.uid

        fun bind(bares: BarRecyclerView, context: Context) {
            nombreBar.text = bares.nombre
            if (bares.imagen != "") {
                Picasso.with(imagenBar.context).load(bares.imagen).into(imagenBar)
            }

            val dialog = Dialog(context)
            dialog.setContentView(R.layout.layout_information_bar)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val dialogNombre = dialog.findViewById(R.id.tv_nombre) as TextView
            val dialogClasificacion = dialog.findViewById(R.id.tv_clasificacion) as TextView
            val dialogDireccion = dialog.findViewById(R.id.tv_direccion) as TextView
            val dialogTelefono = dialog.findViewById(R.id.tv_telefono) as TextView


            favoritoBar.setOnClickListener {
                addFavorito(context, bares.barID, firebaseAuth.toString(),bares.nombre, bares.imagen, bares.estrellas, bares.direccion, bares.telefono)
            }

            verBar.setOnClickListener {
                dialogNombre.text = bares.nombre
                dialogClasificacion.text = bares.estrellas.toString()
                dialogDireccion.text = bares.direccion
                dialogTelefono.text = bares.telefono
                dialog.show()
            }
        }

        private fun addFavorito(context: Context, cod: String, usu: String, nomb: String, ima: String, est: Number, dir: String, tel: String) {
            val admin = AdminSQLiteHelper(context, "administracion", null, 1)
            val bd = admin.writableDatabase
            val fila = bd.rawQuery("select * from favorito where codigo='${cod}' and usuario='${usu}'", null)
            if (fila.moveToFirst()) {
                Toast.makeText(context, "Ya esta como Favorito", Toast.LENGTH_SHORT).show()
            } else {
                val registro = ContentValues()
                registro.put("codigo", cod)
                registro.put("usuario", usu)
                registro.put("nombre", nomb)
                registro.put("imagen", ima)
                registro.put("estrellas", est.toString())
                registro.put("direccion", dir)
                registro.put("telefono", tel)
                bd.insert("favorito", null, registro)
                Toast.makeText(context, "Se añadio a Favoritos", Toast.LENGTH_SHORT).show()
            }
            bd.close()
        }
    }
}