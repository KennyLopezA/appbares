package com.example.appbares.model

class BarRecyclerView (
    var barID: String,
    var nombre: String,
    var imagen: String,
    var estrellas: Number,
    var direccion: String,
    var telefono: String
) {
    override fun toString(): String {
        return nombre
    }

    companion object {
        @JvmStatic
        fun newInstance() = BarRecyclerView("","", "", 0, "", "").apply {  }
    }
}