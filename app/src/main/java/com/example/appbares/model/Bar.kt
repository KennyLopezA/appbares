package com.example.appbares.model

import com.google.firebase.firestore.GeoPoint

class Bar {
    var nombre: String = ""
    var coordenada: GeoPoint? = null
}